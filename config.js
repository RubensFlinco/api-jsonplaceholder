module.exports = {
    server: {
        port: 80
    },
    api: {
        baseUrl: "https://jsonplaceholder.typicode.com"
    }
}