module.exports = (lista, chave, ordem) => {
    return lista.sort(function(a, b) {
        var x = a[chave]; var y = b[chave];
        if (ordem.toLowerCase() === 'asc' ) { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
        if (ordem.toLowerCase() === 'desc') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
    });
}