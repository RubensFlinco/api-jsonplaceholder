const axios = require('axios');
const config = require('../../config');
const funSort = require('../../functions/sort');

class ServicesUser {

  getUsers() {
    return new Promise(async (resolve, reject) => {
      let users = await axios({ method: 'GET', url: config.api.baseUrl + '/users' });
      users = users.data;

      if (users) {
        resolve(users);
      } else {
        reject("Não achamos nenhum resultado!");
      }
    });
  }

  getUsersSort(chave, ordem) {
    return new Promise(async (resolve, reject) => {
      let users = await this.getUsers();
      users = funSort(users, chave, ordem);

      if (users) {
        resolve(users);
      } else {
        reject("Não achamos nenhum resultado!");
      }
    });
  }

  async getUser(id) {
    return new Promise(async (resolve, reject) => {
      let users = await this.getUsers();
      users = users.filter(function (user) {
        return parseInt(user.id) === parseInt(id);
      });
      users = users[0];

      if (users) {
        resolve(users);
      } else {
        reject("Não achamos nenhum resultado!");
      }
    });
  }

  async getUserFilterSuite(boolean) {
    return new Promise(async (resolve, reject) => {
      if (boolean == "true") {
        boolean = true;
      }else
      if (boolean == "false") {
        boolean = false;
      }
      let users = await this.getUsers();
      users = users.filter(function (user) {
        let e = user.address.suite.toLowerCase();
        return e.includes('suite') === boolean;
      });

      if (users) {
        resolve(users);
      } else {
        reject("Não achamos nenhum resultado!");
      }
    });
  }

}

module.exports = ServicesUser;