# Dependencias
- Vagrant
- VirtualBox

# Instalação incial
- Você precisa ter instalado o Vagrant & VirtualBox
- Baixar ou ter um clone desse repo
- Entrar na pasta raiz desse repo
- Digitar o comando "vagrant up"
- Pronto só aguardar agora que tudo será instalado na maquina virtual
- Depois de tudo devidamente terminado você pode ver a pagina acessando no seu navegador o endereço http://localhost:8080/

# Quais rotas estão disponiveis ?
- http://localhost:8080/ (MSG para saber que o server está funcionando)
- http://localhost:8080/api/user/get (Retorna todos usuarios)
- http://localhost:8080/api/user/get/(ID) (Retorna apenas o usuario com o id que definiu)
- http://localhost:8080/api/user/get/sort/(KEY)/(ASC|DESC) (Retorna todos usuarios, mas com uma ordem definida pela Chave que passou)
- http://localhost:8080/api/user/get/filter/suite/(TRUE|FALSE) (Retorna todos usuarios, com a condição que você pode definir sendo true contem suite ou false não contem suite no endereço)

# Rotas exemplo
- http://localhost:8080/
- http://localhost:8080/api/user/get
- http://localhost:8080/api/user/get/1
- http://localhost:8080/api/user/get/sort/name/asc
- http://localhost:8080/api/user/get/filter/suite/true

# Quero mexer na maquina virtual como faço?
- Depois de ter feito a instalaçao inical, e estar na pasta raiz desse repo
- Basta digitar "vagrant ssh"
- Pronto você estará dentro do console da maquina virtual

# Quero fechar a maquina virtual como faço ?
- Basta digitar "vagrant suspend"
- Pronto a maquina virtual será destaligada

# Quero deletar a maquina virtual como faço ?
- Basta digitar "vagrant destroy"
- Confirme com "y"
- Pronto a maquina virtual será destaligada e removida

By Rubens Flinco