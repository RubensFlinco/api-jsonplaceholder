var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  let json = {
    msg:"Olá, server rodando.."
  }

  res.json(json);
});

module.exports = router;
