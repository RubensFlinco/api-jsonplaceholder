const express = require('express');
const router = express.Router();
const ServicesUser = require('../services/user/user');


/* GET users listing. */
router.get('/user/get', async function(req, res, next) {
  let json = await ServicesUser.prototype.getUsers();
  res.json(json);
});

/* GET users listing. */
router.get('/user/get/sort/:key/:ordem', async function(req, res, next) {
  let json = await ServicesUser.prototype.getUsersSort(req.params.key, req.params.ordem);
  res.json(json);
});


/* GET users listing. */
router.get('/user/get/:id', async function(req, res, next) {
  let json = await ServicesUser.prototype.getUser(req.params.id);
  res.json(json);
});


/* GET users listing. */
router.get('/user/get/filter/suite/:boolean', async function(req, res, next) {
  let json = await ServicesUser.prototype.getUserFilterSuite(req.params.boolean);
  res.json(json);
});

module.exports = router;
